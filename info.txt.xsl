<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="utf-8"/>
  <xsl:strip-space elements="*" />  
  <xsl:template match="/session">
    * <xsl:value-of select="title"/> *

Speakers: <xsl:value-of select="speakers/@display"/>
Track: <xsl:value-of select="track"/>
Date: <xsl:value-of select="timeslot/@begin"/>

Description:
<xsl:value-of select="description"/><xsl:text>&#xA;</xsl:text>
  </xsl:template>
</xsl:stylesheet>